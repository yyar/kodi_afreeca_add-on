#-*- coding:utf-8 -*-

import sys
import xbmc
import xbmcgui
import xbmcplugin
from resources.lib import api

DEFAULT_KEYWORD = 'nicegametv'

addon_handle = int(sys.argv[1])

keyboard = xbmc.Keyboard(DEFAULT_KEYWORD, 'Input Keyword')
keyboard.doModal()
if keyboard.isConfirmed():
    xbmcplugin.setContent(addon_handle, 'Kodi Afreeca')

    search_result = api.search_afreeca_live(keyboard.getText(), 1)

    for group in search_result['data']['groups']:
        for item in group['contents']:
            url = api.get_hls_url(item['broad_no'])['view_url']

            li = xbmcgui.ListItem(item['title'], iconImage=item['thumbnail'])
            xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)

    xbmcplugin.endOfDirectory(addon_handle)
