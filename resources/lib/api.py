#-*- coding:utf-8 -*-

from request import Request
import json

live_search_url = 'http://sch.afreeca.com/api.php'
#param_example='v=1.0&m=liveSearch&nListCnt=20&isMobile=1&szOrder=&nPageNo=1&szKeyword=fmo&'

headers = {
    'Referer':'http://android.afreeca.com',
    'User-Agent':'kr.co.nowcom.mobile.afreeca/2.0.9 (Android 4.4.2; SHV-E300S) Afreeca API/2.0.9'
}

req = Request()

def search_afreeca_live(keyword, pageNo):
    params = {
        'v':'1.0',
        'm':'liveSearch',
        'nListCnt':20,
        'isMobile':1,
        'szOrder':'',
        'nPageNo':pageNo,
        'szKeyword':keyword
    }

    result = req.post(live_search_url, params, headers)
    return json.loads(result)

live_url = 'http://api.m.afreeca.com/broad/a/list'

#param_example = 'platform=google&current_page=1&order_by_column=view&'
def live_afreeca(pageNo):
    params = {
        'platform':'google',
        'current_page':pageNo,
        'order_by_column':'view'
    }

    result = req.post(live_url, params, headers)
    return json.loads(result)

#url_example
http_streaming_url = 'http://resourcemanager.afreeca.tv:9090/broad_stream_assign.html';
def get_hls_url(broad_id):
    params = {
        'broad_no':'%s-mobile-hd-hls' % broad_id,
        'cors_origin_url':'m.afreeca.com',
        'return_type':'gs_cdn',
        'use_cors':'true'
    }
    result = req.get(http_streaming_url, params)
    return json.loads(result)


if __name__ == "__main__":
    print search_afreeca_live('롤', 1)
    #print live_afreeca(1)
    print get_hls_url('140304852')
