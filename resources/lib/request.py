#!/usr/bin/python
#-*- coding:utf-8 -*-

import urllib, urllib2
import cookielib

class Request(object):
    opener = None
    cookie_jar = None
    logger = None

    def __init__(self, logger=None):
        self.logger = logger
        self.cookie_jar = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cookie_jar))

    def execute(self, url, data=None, headers={}):
        req = urllib2.Request(url=url, data=data, headers=headers)
        r = self.opener.open(req)
        return r.read()

    def get(self, url, param_dict):
        return self.execute('%s?%s' % (url, urllib.urlencode(param_dict)))


    def post(self, url, param_dict, headers):
        return self.execute(url, urllib.urlencode(param_dict), headers)

